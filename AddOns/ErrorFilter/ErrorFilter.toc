﻿## Interface: 30300
## Title: ErrorFilter
## Notes: Filters the errors dislayed in the UIErrorsFrame.
## Version: 2.5.0
## Author: iceeagle (backported by Merfin)
## OptionalDeps: Ace3
## SavedVariables: ErrorFilterDB
## DefaultState: enabled
## X-Curse-Packaged-Version: v2.4.2-2-g85289ba
## X-Curse-Project-Name: ErrorFilter
## X-Curse-Project-ID: errorfilter
## X-Curse-Repository-ID: wow/errorfilter/mainline

## LoadManagers: AddonLoader
## X-LoadOn-Always: Delayed

#@no-lib-strip@
embeds.xml
#@end-no-lib-strip@

Locales\enUS.lua
Locales\deDE.lua
Locales\frFR.lua
Locales\esES.lua
Locales\esMX.lua
Locales\koKR.lua
Locales\ptBR.lua
Locales\ruRU.lua
Locales\zhTW.lua
Locales\zhCN.lua

ErrorFilter.lua
