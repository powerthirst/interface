Parrydetector = LibStub("AceAddon-3.0"):NewAddon("Parrydetector", "AceConsole-3.0", "AceEvent-3.0")

local VERSION = "0.2"

local ENABLED_WHEN_NEVER = 4
local ENABLED_WHEN_RAID = 3
local ENABLED_WHEN_PARTY = 2
local ENABLED_WHEN_ALWAYS = 1
-- [key]=value pair table to choose from, key is the value passed to "set", value is the string displayed
local enabled_when_table = {
   "Always",   -- 1
   "In Party", -- 2
   "In Raid",  -- 3
   "Never"     -- 4
}

local debug = false
local function Debug(message)
   if(debug) then
      Parrydetector:Print(message)
   end
end

-- ----------------------------------------
-- SETUP
-- ----------------------------------------
local myOptionsTable = {
   type = "group",
   args = {
      enable = {
         name = "Enabled",
         type = "select",
         values = enabled_when_table,
         set = function(info, value)
                  Parrydetector:SetEnabled(info, value)
               end,
         get = function(info)
                  return Parrydetector:GetEnabled(info)
               end,
         style = "dropdown"
      },   
      enableTell = {
         name = "Enable Tell",
         desc = "Enables / disables tells on parry",
         type = "toggle",
         set = function(info, value)
                  Parrydetector:SetTell(info, value)
               end,
         get = function(info)
                  return Parrydetector:GetTell(info)
               end
      },
      enableRaid = {
         name = "Enable Raid",
         desc = "Enables / disables raid message on parry",
         type = "toggle",
         set = function(info, value)
                  Parrydetector:SetRaid(info, value)
               end,
         get = function(info)
                  return Parrydetector:GetRaid(info)
               end
      },
      enableLocal = {
         name = "Enable Local",
         desc = "Enables / disables local message on parry",
         type = "toggle",
         set = function(info, value)
                  Parrydetector:SetLocal(info, value)
               end,
         get = function(info)
                  return Parrydetector:GetLocal(info)
               end
      },
      report = {
         name = "Report",
         desc = "Report on the current settings for this addon",
         type = "execute",
         func = function() Parrydetector:Report() end
      }
   }
}

function Parrydetector:OnInitialize()
   Debug("Initializing")
  
   -- Code that you want to run when the addon is first loaded goes here.
   LibStub("AceConfig-3.0"):RegisterOptionsTable("Parrydetector", myOptionsTable, {"parrydetector", "pd"})
   self.db = LibStub("AceDB-3.0"):New("ParrydetectorDB")

   if(self.db.global.enabled == nil) then
      self.db.global.enabled = ENABLED_WHEN_RAID
   end
   if(self.db.global.enable_tell == nil) then
      self.db.global.enable_tell = false
   end
   if(self.db.global.enable_raid == nil) then
      self.db.global.enable_raid = false
   end
   if(self.db.global.enable_local == nil) then
      self.db.global.enable_local = true
   end

   Debug("Initialized")
end

function Parrydetector:OnEnable()
   -- Called when the addon is enabled
   self:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
   self:Print("ParryDetector is currently enabled " .. tostring(enabled_when_table[self.db.global.enabled]))
end

function Parrydetector:OnDisable()
   -- Called when the addon is disabled
   self:UnregisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
   self:Print("ParryDetector disabled.")
end

function Parrydetector:COMBAT_LOG_EVENT_UNFILTERED(...)
   local eventType, sourceGUID, sourceName, sourceFlags,destGUID, destName, _, prefix1, prefix2, prefix3 = select(3,...)
   local PlayerGUID = UnitGUID("player")
   if (eventType == "SWING_MISSED" and prefix1 == "PARRY" and destGUID == UnitGUID("target")) then
      Parrydetector:Announce("PARRY",sourceName, sourceGUID, destName, destGUID)
   end
end
--sourceGUID ~=PlayerGUID and
-- /script Parrydetector:setDebug(false)
-- /script Parrydetector:setDebug(true)
function Parrydetector:setDebug(value)
   debug = value
end
-- ----------------------------------------
-- Overrides
-- ----------------------------------------
-- These work for players, but not for their pets... srsly, wtf
local unitOrPetInParty = UnitPlayerOrPetInParty
function Parrydetector:UnitPlayerOrPetInParty(unitName)
   Debug("Checking if " .. tostring(unitName) .. " is in your party " .. tostring(unitOrPetInParty(unitName)))
   return unitOrPetInParty(unitName) == 1
end

local unitOrPetInRaid = UnitPlayerOrPetInRaid
function Parrydetector:UnitPlayerOrPetInRaid(unitName)
   Debug("Checking if " .. tostring(unitName) .. " is in your raid " .. tostring(unitOrPetInRaid(unitName)))
   return unitOrPetInRaid(unitName) == 1
end

-- Returns true if the unitGUID is for a player character, false otherwise
-- /script Parrydetector:Print(tostring(Parrydetector:IsPlayer(UnitGUID("target"))))
function Parrydetector:IsPlayer(unitGUID)
   --name = Parrydetector:Print(GetPlayerInfoByGUID(tostring(UnitGUID("target"))))
   return (GetPlayerInfoByGUID(unitGUID) ~= nil)
end

-- ----------------------------------------
-- ACCESSORS
-- ----------------------------------------
function Parrydetector:SetEnabled(info,val)
   self.db.global.enabled = val
   Parrydetector:Print("now enabled " .. tostring(enabled_when_table[val]))
end
function Parrydetector:GetEnabled(info)
   return self.db.global.enabled
end

function Parrydetector:SetTell(info,val)
   self.db.global.enable_tell = val
   Parrydetector:Print("Tell Messages are now "..tostring(val))
end
function Parrydetector:GetTell(info)
   return self.db.global.enable_tell
end

function Parrydetector:SetRaid(info,val)
   self.db.global.enable_raid = val
   Parrydetector:Print("Raid Messages are now "..tostring(val))
end
function Parrydetector:GetRaid(info)
   return self.db.global.enable_raid
end

function Parrydetector:SetLocal(info,val)
   self.db.global.enable_local = val
   Parrydetector:Print("Local Messages are now "..tostring(val))
end
function Parrydetector:GetLocal(info)
   return self.db.global.enable_local
end


function Parrydetector:in_raid()
   return (GetNumRaidMembers() > 0)
end

function Parrydetector:in_party()
   return (GetNumPartyMembers() > 0)
end

function Parrydetector:addon_is_enabled()
   return((self.db.global.enabled <= 1) or
      (self:in_party() and self.db.global.enabled <= 2) or
      (self:in_raid() and self.db.global.enabled <= 3))
end

-- Returns true if the named thing is the player, or the player's pet
function Parrydetector:is_me_or_my_pet(unitGUID)
   playerGUID = UnitGUID("player")
   petGUID = UnitGUID("pet")
   --Debug("Checking if " .. tostring(unitGUID) .. " is me (" .. tostring(playerGUID) .. ") or my pet (" .. tostring(petGUID) ..")")
   ispet = (unitGUID == petGUID) --UnitIsUnit(unitName, UnitGUID("pet"))
   isme = (unitGUID == playerGUID) --UnitIsUnit(unitName, UnitGUID("player"))
   Debug("is_me_or_my_pet -> " .. tostring(((ispet or isme) == true)))
   return ((ispet or isme) == true)
end

-- returns true if we care whether this unit causes a parry
-- TODO: not working if unit == me or my pet
function Parrydetector:should_report_unit(unitName, unitGUID)
   -- if it's enabled for raid, the player must be in our raid
   if (self.db.global.enabled == ENABLED_WHEN_RAID and self:in_raid() and
       (self:UnitPlayerOrPetInRaid(unitName) or self:is_me_or_my_pet(unitGUID))) then
      Debug("should_report_unit -> true")
      return true
   end
   -- if it's enabled for party, then the player must be in our raid or party
   if (self.db.global.enabled == ENABLED_WHEN_PARTY and
       (self:UnitPlayerOrPetInRaid(unitName) or self:UnitPlayerOrPetInParty(unitName) or self:is_me_or_my_pet(unitGUID))) then
      Debug("should_report_unit -> true")
      return true
   end
   -- if it's enabled always, then the player must be in our raid or party
   if (self.db.global.enabled == ENABLED_WHEN_ALWAYS and
       (self:UnitPlayerOrPetInParty(unitName) or self:UnitPlayerOrPetInRaid(unitName) or self:is_me_or_my_pet(unitGUID))) then
      Debug("should_report_unit -> true")
      return true
   end
   -- only other option is enabled never
   Debug("should_report_unit -> false")
   return false
end

-- /script Parrydetector:Announce("PARRY", "Berfert", UnitGUID("player"), "Berfert", UnitGUID("player"))
-- /script code,results = pcall("Parrydetector:Announce", "Berfert", UnitGUID("player"), "Berfert", UnitGUID("player")) ; Parrydetector:Print("code: " .. tostring(code) .. " results: " .. tostring(results))
function Parrydetector:Announce(type, sourceName, sourceGUID, targetName, targetGUID)
   if (type == "PARRY") then
      Debug("event: " .. tostring(type) .. " -> " .. tostring(sourceName) .. " -> " .. tostring(targetName))
      Debug("addon_is_enabled() : " .. tostring(self:addon_is_enabled()))
      Debug("self:should_report_unit(" .. tostring(sourceName) .. ", " .. tostring(sourceGUID) .. " : " .. tostring(self:should_report_unit(sourceName, sourceGUID)))
      if(self:addon_is_enabled() and self:should_report_unit(sourceName, sourceGUID)) then
         self:AnnounceLocal(sourceName, sourceGUID, targetName, targetGUID)
         self:AnnounceTell(sourceName, sourceGUID, targetName, targetGUID)
         self:AnnounceRaid(sourceName, sourceGUID, targetName, targetGUID)
      end
   end
end

-- /script Parrydetector:AnnounceTell("Berfert", UnitGUID("player"), "Berfert", UnitGUID("player"))
function Parrydetector:AnnounceTell(sourceName, sourceGUID, targetName, targetGUID)
   Debug("Sending tell to " .. tostring(sourceName) .. "?")
   if (self.db.global.enable_tell and self:IsPlayer(sourceGUID)) then
      Debug("Sending tell to " .. tostring(sourceName) .. " yes")
      local s = "Automated Report You just caused " .. targetName .. " to parry!"
      SendChatMessage(s ,"WHISPER", nil, sourceName);
   else
      Debug("Sending tell to " .. tostring(sourceName) .. " no")
   end
   -- TODO: Send tell to master if unit is a pet
end

-- /script Parrydetector:AnnounceRaid("Berfert", UnitGUID("player"), "Berfert", UnitGUID("player"))
function Parrydetector:AnnounceRaid(sourceName, sourceGUID, targetName, targetGUID)
   Debug("Sending raid about " .. tostring(sourceName) .. "?")
   if (self.db.global.enable_raid and self:in_raid()) then
      Debug("Sending raid about " .. tostring(sourceName) .. " yes")
      local s = sourceName .. " has caused a PARRY by " .. targetName .."!"
      SendChatMessage(s ,"RAID");
   else
      Debug("Sending raid about " .. tostring(sourceName) .. " no")
   end
end

-- /script Parrydetector:AnnounceLocal("Berfert", UnitGUID("player"), "Berfert", UnitGUID("player"))
function Parrydetector:AnnounceLocal(sourceName, sourceGUID, targetName, targetGUID)
   --self:Print("Sending local")
   if (self.db.global.enable_local) then
      local s = sourceName .. " has caused a PARRY by " .. targetName .. "!"
      self:Print(s)
   end
end

function Parrydetector:Report()
   self:Print("ParryDetector Configuration:" ..
              "\n    Enabled: " .. tostring(enabled_when_table[self.db.global.enabled]) ..
                 "\n    Enable Tells: " .. tostring(self.db.global.enable_tell) ..
                 "\n    Enable Raid Message: " .. tostring(self.db.global.enable_raid) ..
                 "\n    Enable Local Message: " .. tostring(self.db.global.enable_local))
end

-- /script Parrydetector:DebugReport()
function Parrydetector:DebugReport()
   self:Report()
   targetGUID = UnitGUID("target")
   targetName = UnitName("target")

   self:Print("ParryDetector Debug:" ..
              "\n    You are in a group: " .. tostring(self:in_party()) ..
                 "\n    You are in a raid: " .. tostring(self:in_raid()) ..
                 "\n    " .. targetName .. " is you or your pet: " .. tostring(self:is_me_or_my_pet(targetGUID)) ..
                 "\n    " .. targetName .. " is in your group: " .. tostring(self:UnitPlayerOrPetInParty(targetName)) ..
                 "\n    " .. targetName .. " is in your raid: " .. tostring(self:UnitPlayerOrPetInRaid(targetName)) ..
                 "\n    should report on " .. targetName .. ": " .. tostring(self:should_report_unit(targetName, targetGUID)) ..
                 "\n    addon will report at all: " .. tostring(self:addon_is_enabled()))

end